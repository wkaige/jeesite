package com.jeesite.modules.sys.database.web;

import com.jeesite.common.collect.ListUtils;
import com.jeesite.common.lang.StringUtils;
import com.jeesite.common.web.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "${adminPath}/database")
public class DatabaseController extends BaseController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Form
     */
    @RequiresPermissions("test:testData:view")
    @RequestMapping(value = "form")
    public String form(Model model) {
        model.addAttribute("testData", "testData");
        return "modules/sys/databaseForm";
    }

    /**
     * 查询列表数据
     */
    @RequiresPermissions("test:testData:view")
    @RequestMapping(value = "select")
    @ResponseBody
    public List<Map<String, Object>> select(String sql) {
        if (StringUtils.isBlank(sql) || !sql.trim().toLowerCase().startsWith("select")) {
            return new LinkedList<>();
        }
        sql = StringUtils.replaceAll(sql, "＇", "'");
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        if (ListUtils.isNotEmpty(list)) {
            return list;
        } else {
            return new LinkedList<>();
        }
    }

    @RequiresPermissions("test:testData:view")
    @RequestMapping(value = "update")
    @ResponseBody
    public Integer update(String sql) {
		if (StringUtils.isBlank(sql) || !sql.trim().toLowerCase().startsWith("update")) {
			return 0;
		}
        sql = StringUtils.replaceAll(sql, "＇", "'");
		return jdbcTemplate.update(sql);
	}

    @RequiresPermissions("test:testData:view")
    @RequestMapping(value = "insert")
    @ResponseBody
    public Integer insert(String sql) {
		if (StringUtils.isBlank(sql) || !sql.trim().toLowerCase().startsWith("insert")) {
			return 0;
		}
        sql = StringUtils.replaceAll(sql, "＇", "'");
		return jdbcTemplate.update(sql);
	}

    @RequiresPermissions("test:testData:view")
    @RequestMapping(value = "delete")
    @ResponseBody
    public Integer delete(String sql) {
		if (StringUtils.isBlank(sql) || !sql.trim().toLowerCase().startsWith("delete")) {
			return 0;
		}
        sql = StringUtils.replaceAll(sql, "＇", "'");
		return jdbcTemplate.update(sql);
    }

}